package com.assignmentOne.extraction.util

object Address {
  case class Address(country: String,
                     region: String,
                     city: String,
                     postcode: String,
                     road: String,
                     roadNumbers: String)
}
